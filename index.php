<!DOCTYPE html>
<html  style="">

<head>
    <title>Lorem Ipsum</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=0.5">
    <link rel="stylesheet" href="css/main.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/animate.css">
    <link rel="icon" href="ISO.gif" />
    <link rel="stylesheet" type="text/css" href="grid/simplegrid.css">
    <script src="https://use.fontawesome.com/ee0e2cc06d.js"></script>
    <!--<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>-->
    <script src="js/jquery.js"></script>
    <script src="js/app.js"></script>
</head>

<body style="">
    <!-- top -->
    <div class="top">
        
        <button style="float:right;display:inline-block" class="back">Retour</button>
        <h1>Lörem Ipsüm</h1>
        <div class="fb-like" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-action="like" data-size="small" data-show-faces="" data-share="" style="line-height: 60px;transform: scale(0.8);"></div>
        <div class="bar"></div>
    </div>
    <!-- fin top -->
    <!-- content -->
    <div class="content">
        <div class="right">
            <h2 class="roboto">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque augue est, egestas vitae quam ac, commodo rutrum purus. Duis ut turpis maximus, consectetur leo at, ultrices risus. Nam pharetra nec sem ut condimentum. Nam ornare aliquet enim. Curabitur suscipit quam nisl, vel tristique tortor ullamcorper non. Nam ultricies cursus aliquam. Suspendisse pharetra lorem et neque convallis, sed vulputate lectus euismod. Mauris magna ipsum, pulvinar sit amet faucibus vel, tincidunt eu lorem.</h2>
            <button id="start"> Lorem >> </button>
        </div>
        <div class="menu">
            <div class="grid">
                <div class="col-6-12">
                    <div data-pic="Rappeurs" data-src="rappeurs" class="pic-low rappeurs"></div>
                </div>
                <div class="col-3-12">
                    <div data-pic="Actualité" data-src="news" class="pic-low actu"> </div>
                </div>
                <div class="col-3-12">
                    <div data-pic="coups de Keür" data-src="keur" class="pic-low keur"> </div>
                </div>
            </div>
            <div class="grid">
                <div class="col-3-12">
                    <div data-pic="Rap Fighter" data-src="fighter" class="pic-low bende"> </div>
                </div>
                <div class="col-6-12">
                    <div data-pic="Crew Mapping" class="pic-low crew"> </div>
                </div>
                <div class="col-3-12">
                    <div data-pic="Classement" class="pic-low nek"> </div>
                </div>
            </div>
            <div class="grid">
                <div class="col-8-12">
                    <div id="video-grunt" data-video="true" data-pic="listen Grünt 31" data-src="https://www.youtube.com/embed/gW_7RmpUZOE" class="pic grunt"> </div>
                </div>
                <div class="col-4-12">
                    <div data-pic="Notre Playlist" data-src="playlist" class="pic mapping"> </div>
                </div>
            </div>
        </div>
        <!-- here is the real content information when we click on a button's menu -->
        <div id="content">
            
        </div>
        <!-- end real content -->
        
    </div>
    <!-- fin content -->
    <footer>Lörem Ipsüm - <a href="#contact">Contact</a> 
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
   <script>
     var s = "";
    $(document).keypress(function(event){
          s += (String.fromCharCode(event.which));
          console.log(s.length);
          if (s.length == 3) {	
                if (s == "mdp") {
                    document.location.href= "/Admin";
                 }
          }
    });    
    </script>
    </footer>
</body>

</html>