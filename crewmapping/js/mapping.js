var BDD = [
  {
    id: 1,
    blaze: "Lesram",
    ville: "Pré-Saint-Gervais",
    connexions: [2,3,4]
  },
  {
    id: 2,
    blaze: "Lu'Cid",
    ville: "Paris 20e",
    connexions: [1,3]
  },
  {
    id: 3,
    blaze: "S-Cap",
    ville: "Paris 19e",
    connexions: [1,2]
  },
  {
    id: 4,
    blaze: "PLK",
    ville: "Paris",
    connexions: [1,3]
  }
];

class Node {
  constructor(id,type,name,adj) {
    this.id = id;
    this.type = type;
    this.name = name;
    this.adj = adj;
  }
}

class Graph {
  constructor(type,nodes,adjList) {
    this.type = type;
    this.order = nodes.length;
    this.nodes = nodes;
    this.adjList = adjList;
  }
}

function makeGraph(bdd) {
  nodes = [];
  adjList = [];
  for(var element in bdd) {
    e = bdd[element];
    nodes.push(new Node(e.id, "", e.blaze,e.connexions));
    adjList.push(e.connexions);
  }
  return new Graph("",nodes,adjList);
}

G = makeGraph(BDD);

M = [false,false,false,false];

function depth(g,m,s) {
  m[s.id-1] = true;
  alert(s.name + " " + s.id);
  for(var adj in g.adjList[s.id - 1]) {
    //console.log(g.adjList[s.id - 1][adj].name);
    if(!m[g.adjList[s.id - 1][adj] - 1]) {
      depth(g,m,g.nodes[g.adjList[s.id - 1][adj] - 1]);
    }
  }
}

depth(G,M,G.nodes[0]);
