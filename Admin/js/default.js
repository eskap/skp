$(function () {
    var scriptLoaded = 0;
    var nb = [1];
    var path = "";
    var p_crew = [];
    var deleted_crew = "";
    var p_pseudo = "";

    //if() {
    //  alert("test");
    //}

    $('html').on("click",'#add_c',function (e) {
        //nb = $('.crews').data('number');
        if(nb < 18){
            nb[0] += 1;
            $('<div><input id="in" class="col col-md-3" type="text" name="nb'+nb[0]+'" placeholder="Crew"></div>').insertBefore('#add_c');
            $('input[name="nb'+(nb[0]-1)+'"]').parent().append('<button id="delete_c"> X </button>');
            $('.crews').attr('data-number', nb[0]);
        }
        if(nb == 18){
            $('input[name="nb18"]').parent().append('<button id="delete_c"> X </button>');
            $('#add_c').hide();
        }
    });
    $('html').on("click", '#delete_c', function (e) {
        e.preventDefault();
        $('.alert-warning').empty();
        $('.alert-success').empty();
        $('.alert-error').empty();
        var page = $('li[name=gerer]').attr('class');
        if(page.includes("active")) {
            $.get('php/res_req_manage.php',{
                type: 'delete_crew',
                pseudo: p_pseudo,
                crew: deleted_crew
            }, function(data) {
                var err = data.erreurs;
                if(!err)
                {
                    for(var i = 0; i < err.length; i++) {
                      $('.alert-error').attr('class','alert alert-error');
                      $('.alert-error').html(err[i]);
                    }
                } else {
                    $('.alert-success').attr('class','alert alert-success');
                    $('.alert-success').html(' Crew deleted successfully');
                }
            }, "json");
        }
        if(nb < 18)
        {
            $('#add_c').show();
        } else {
            $('input[name="nb17"]').parent().children().last().remove();
        }
        nb[0] -= 1;
        $(this).parent().remove();
        var index = 1;
        for(var j = 1; j <= 18; ++j)
        {
            if($('input[name="nb'+j+'"]').length > 0)
            {
                $('input[name="nb'+j+'"]').attr('name','nb'+index);
                index += 1;
            }
        }
        if(nb[0] == 0) {
            nb[0] = 1;
            $('#in').attr('name','nb1');
        }
        $('.crews').attr('data-number', nb[0]);
    });
    $('html').on('click','#add', function (e) {
        e.preventDefault();
        $('.alert-warning').empty();
        $('.alert-success').empty();
        $('.alert-error').empty();
        pseudo = $('input[name=pseudo]').val();
        prenom = $('input[name=prenom]').val();
        crew = [];
        for(var i = 1; i <= nb[0]; i++) {
            crew[i-1] = $('input[name=nb'+i+']').val();
        }
        if(!document.getElementById("search")) {
            $.get('php/res_req_add.php',{
                type: 'add_rappeur',
                pseudo: pseudo,
                prenom: prenom,
                crew: crew
            }, function(data) {
                var err = data.erreurs;
                if(!err)
                {
                    for(var i = 0; i < err.length; i++) {
                      $('.alert-error').attr('class','alert alert-error');
                      $('.alert-error').html(err[i]);
                    }
                } else {
                    $('.alert-success').attr('class','alert alert-success');
                    $('.alert-success').html(' Rapper added successfully');
                    //$('.alert-success').attr('class','alert alert-success');
                    //$('.alert-success').append(' Rapper updated successfully');
                    $('input[name=pseudo]').val("");
                    $('input[name=prenom]').val("");
                    for(var i = nb[0]; i > 1; i--) {
                        $('input[name=nb'+i+']').parent().remove();
                    }
                    if(nb[0] > 1) {
                        $('input[name=nb1]').parent().children().last().remove();
                    }
                    nb[0] = 1;
                    $('input[name=nb1]').val("");
                }
            }, "json");
        } else {
            $.get('php/res_req_manage.php',{
                type: 'update_rappeur',
                p_pseudo: p_pseudo,
                p_name: p_name,
                p_crew: p_crew,
                pseudo: pseudo,
                prenom: prenom,
                crew: crew
            }, function(data) {
                var err = data.erreurs;
                if(err)
                {
                    for(var i = 0; i < err.length; i++) {
                      $('.alert-error').attr('class','alert alert-error');
                      $('.alert-error').html(err[i]);
                    }
                } else {
                    $('.alert-success').attr('class','alert alert-success');
                    $('.alert-success').html(' Rapper updated successfully');
                    $('input[name=search]').val("");
                    $('input[name=pseudo]').val("");
                    $('input[name=prenom]').val("");
                    for(var i = nb[0]; i > 0; i--) {
                        $('input[name=nb'+i+']').parent().remove();
                    }
                    nb[0] = 0;
                    $('#d_button').hide();
                    $('#add_c').hide();
                    $('input[name=pseudo]').hide();
                    $('input[name=prenom]').hide();
                    $('#add').hide();
                }
            }, "json");
        }
    });
    $('html').on('click','a', function (e) {
        e.preventDefault();
        path = $(this).data('href');
        $('li[name=accueil]').attr('class','col col-md-3');
        $('li[name=ajouter]').attr('class','col col-md-3');
        $('li[name=gerer]').attr('class','col col-md-3');
        $('li[name=soumission]').attr('class','col col-md-3');
        $('.content').load(path);
        $(this).parent().attr('class','col col-md-3 active');
    });
    $('html').on('click','#search', function (e) {
        e.preventDefault();
        $('.alert-warning').empty();
        $('.alert-success').empty();
        $('.alert-error').empty();
        pseudo = $('input[name=search]').val();
        $.get('php/res_req_manage.php', {
            type: 'gerer_rapper',
            pseudo: pseudo
        }, function(data) {
            $('input[name=search]').val("");
            $('input[name=pseudo]').val("");
            $('input[name=prenom]').val("");
            for(var i = nb[0]; i > 0; i--) {
                $('input[name=nb'+i+']').parent().remove();
            }
            nb[0] = 0;
            $('#d_button').hide();
            $('#add_c').hide();
            $('input[name=pseudo]').hide();
            $('input[name=prenom]').hide();
            $('#add').hide();
            var err = data.erreurs;
            if(!err)
            {
                $('#add').show();
                $('input[name=pseudo]').show();
                $('input[name=prenom]').show();
                $('#add_c').show();
                $('#d_button').show();
                var c = data.crew;
                p_crew = c;
                nb[0] = c.length;
                if(nb[0] > 0)
                {
                    $('input[name=pseudo]').val(data.pseudo);
                    p_pseudo = $('input[name=pseudo]').val();
                    $('input[name=prenom]').val(data.prenom);
                    p_name = $('input[name=prenom]').val();
                    $('.crews').attr('data-number',nb[0]);
                    $('<div><input id="in" class="col col-md-3" type="text" name="nb'+(1)+'"></div>').insertBefore('#add_c');
                    $('input[name=nb'+(1)+']').val(c[0]);
                    $('input[name=nb1]').val(c[0]);
                    for(var i = 1; i < nb[0]; i++)
                    {
                        $('input[name=nb'+i+']').parent().append('<button id="delete_c"> X </button>');
                        $('<div><input id="in" class="col col-md-3" type="text" name="nb'+(i+1)+'" placeholder="Crew"></div>').insertBefore('#add_c');
                        $('input[name=nb'+(i+1)+']').val(c[i]);
                    }
                    $('input[name=nb'+nb[0]+']').parent().append('<button id="delete_c"> X </button>');
                    //$('#d_button').prop('disabled',false);
                } else {
                    $('.alert-warning').attr('class','alert alert-warning');
                    $('.alert-warning').html('Rapper not found');
                }
            } else {
                for(var i = 0; i < err.length; i++) {
                  $('.alert-error').attr('class','alert alert-error');
                  $('.alert-error').html(err[i]);
                }
            }
        }, "json");
    });
    $('html').on('click','#d_button', function(e) {
        e.preventDefault();
        $('.alert-warning').empty();
        $('.alert-success').empty();
        $('.alert-error').empty();
        $.get('php/res_req_manage.php',{
            type: 'delete_rapper',
            p_pseudo: p_pseudo,
            p_name: p_name,
            p_crew: p_crew
        }, function(data) {
            var err = data.erreurs;
            if(err)
            {
                for(var i = 0; i < err.length; i++) {
                  $('.alert-error').attr('class','alert alert-error');
                  $('.alert-error').html(err[i]);
                }
            } else {
                $('.alert-success').attr('class','alert alert-success');
                $('.alert-success').html(' Rapper deleted successfully');
                //$('.alert-success').attr('class','alert alert-success');
                //$('.alert-success').append(' Rapper updated successfully');
                $('input[name=search]').val("");
                $('input[name=pseudo]').val("");
                $('input[name=prenom]').val("");
                for(var i = nb[0]; i > 0; i--) {
                    $('input[name=nb'+i+']').parent().remove();
                }
                nb[0] = 0;
                $('#d_button').hide();
                $('#add_c').hide();
                $('input[name=pseudo]').hide();
                $('input[name=prenom]').hide();
                $('#add').hide();
            }
        }, "json");
    })
});
