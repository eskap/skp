<div>
    <div class="alert alert-success alert-display">
        <strong>Success!</strong>
    </div>
    <div class="alert alert-info alert-display">
        <strong>Info!</strong>
    </div>
    <div class="alert alert-error alert-display">
        <strong>Error!</strong>
    </div>
    <div class="alert alert-warning alert-display">
        <strong>Warning!</strong>
    </div>
    <input class="col col-md-3" type="text" name="pseudo" placeholder="Pseudo">
    <div class="col col-md-1"></div>
    <input class="col col-md-3" type="text" name="prenom" placeholder="Prénom" style="margin-left:-5px;"><br><br>
    <div class="crews" data-number="1">
        <div>
            <input id="in" class="col col-md-3" type="text" name="nb1" placeholder="Crew">
        </div>
        <button id="add_c"> + </button><br>
    </div>
    <button id="add">Valider</button>
</div>
