<!DOCTYPE html>
<html>
    <head>
        <title>Titre</title>
        <meta charset="utf-8">
        <!-- Charger CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link type="text/css" rel="stylesheet" href="Style/default.css">

         <script
          src="https://code.jquery.com/jquery-3.1.1.min.js"
          integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8="
          crossorigin="anonymous"></script>
        <script src="js/default.js"></script>
    </head>
    <body>
        <div class="main">
            <nav class="navbar navbar-default">
                <div class="container-fluid" id="no-padding">
                    <ul class="nav navbar-nav">
                        <li class="col col-md-3 active" id="no-padding" name="accueil"><a data-href="Global/acceuil.php">Acceuil</a></li>
                        <li class="col col-md-3" id="no-padding" name="soumission"><a data-href="Global/soumission.php">Soumissions</a></li>
                        <li class="col col-md-3" id="no-padding" name="ajouter"><a data-href="Global/ajouter.php">Ajouter</a></li>
                        <li class="col col-md-3" id="no-padding" name="gerer"><a data-href="Global/gerer.php">Gérer</a></li>
                    </ul>
                </div>
            </nav>
            <div class="content">
