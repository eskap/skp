<?php

//Variables pour la base de données
define('SQL_DSN', 'mysql:dbname=test;host=localhost'); //3306
define('SQL_USERNAME', 'root');
define('SQL_PASSWORD', '');

//Autres variables
define('CHEMIN_LIB','libs/');
define('CHEMIN_VUES_GLOBALES','Global/');
define('CHEMIN_MODELE','Modeles/');
define('CHEMIN_CONTROLLER','Controllers/');