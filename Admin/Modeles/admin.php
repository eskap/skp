<?php
function add_rapper($prenom,$pseudo,$crew,&$erreurs_array) {
    $pdo = PDO2::getInstance();
    $id = 0;

    $requete = $pdo->prepare("INSERT INTO rappers SET
        prenom = :prenom,
        pseudo = :pseudo");
    try {
        $requete->bindValue(':prenom',$prenom);
        $requete->bindValue(':pseudo',$pseudo);
        if($requete->execute()) {
            $id = $pdo->lastInsertId();
        }
    } catch (PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à rappers échouée</center>";
    }
    if($id != 0) {
        foreach($crew as $c){
            $requete = $pdo->prepare("INSERT INTO crew SET
                rapper = :rapper,
                nom = :nom");
            try {
                $requete->bindValue(':rapper',$id);
                $requete->bindValue(':nom',$c);
                $requete->execute();
            } catch (PDOException $ex) {
                $erreurs_array[] = "<center>Erreur: Connexion à crew échouée</center>";
            }
        }
        if(count($erreurs_array) == 0)
        {
            return $id;
        }
    }
    return $requete->errorInfo();
}

function get_rapper($pseudo, &$prenom, &$crew, &$erreurs_array) {
    $pdo = PDO2::getInstance();
    $id = 0;
    $requete = $pdo->prepare("SELECT id,pseudo,prenom FROM rappers WHERE
                                pseudo = :pseudo");
    try{
        $requete->bindValue(':pseudo',$pseudo);
        $requete->execute();
    } catch (PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à rappers échouée</center>";
    }
    if($result = $requete->fetch(PDO::FETCH_ASSOC)) {
		$requete->closeCursor();
        $id = $result['id'];
        $prenom = $result['prenom'];
	}
    if($id != 0) {
        $requete = $pdo->prepare("SELECT nom FROM crew WHERE
                                    rapper = :rapper");
        try {
            $requete->bindValue(':rapper',$id);
            $requete->execute();
        } catch (PDOException $ex) {
            $erreurs_array[] = "<center>Erreur: Connexion à crew échouée</center>";
        }
        while($result = $requete->fetch(PDO::FETCH_ASSOC)) {

            //if(!is_array($result['nom'])){
            //    $crew[] = $result['nom'];
            //} else {
            $crew[] = $result['nom'];
            //}

        }
        $requete->closeCursor();
        if(count($erreurs_array) == 0)
        {
            return $id;
        }
    }
    return $requete->errorInfo();
}

function delete_rapper($p_pseudo, $p_name, $p_crew, &$erreurs_array) {
    $pdo = PDO2::getInstance();
    $id = 0;
    $requete = $pdo->prepare("Select id FROM rappers WHERE
                                pseudo = :pseudo AND
                                prenom = :prenom");
    try {
        $requete->bindValue(':pseudo',$p_pseudo);
        $requete->bindValue(':prenom',$p_name);
        $requete->execute();
    } catch(PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à rapper échouée</center>";
    }
    if($result = $requete->fetch(PDO::FETCH_ASSOC)) {
        $requete->closeCursor();
        $id = $result['id'];
    }
    if($id > 0) {
        foreach($p_crew as $c) {
            delete_crew($id,$c,$erreurs_array);
        }
        $requete = $pdo->prepare("DELETE FROM rappers
        WHERE id = :id");
        try {
            $requete->bindValue(':id', $id);
            $requete->execute();
        } catch (PDOException $ex) {
            $erreurs_array[] = "<center>Erreur: Connexion à rappers échouée</center>";
        }
        if(count($erreurs_array) != 0) {
            return $id;
        }
        return $requete->errorInfo();
    }
    return $requete->errorInfo();
}

function delete_crew($id,$crew,&$erreurs_array) {
    $pdo = PDO2::getInstance();

    $requete = $pdo->prepare("DELETE FROM crew
        WHERE rapper = :rapper AND nom = :nom");
    try {
        $requete->bindValue(':rapper', $id);
        $requete->bindValue(':nom',$crew);
        $requete->execute();
    } catch (PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à crew échouée</center>";
    }
    if(count($erreurs_array) == 0)
    {
        return $id;
    }
    return $requete->errorInfo();
}

function update_rapper($p_pseudo,$p_name,$p_crew,$prenom,$pseudo,$crew,&$erreurs_array) {
    $pdo = PDO2::getInstance();
    $id = 0;

    $requete = $pdo->prepare("UPDATE rappers SET
        prenom = :prenom,
        pseudo = :pseudo
        WHERE pseudo = :p_pseudo");
    try {
        $requete->bindValue(':p_pseudo',$p_pseudo);
        $requete->bindValue(':prenom',$prenom);
        $requete->bindValue(':pseudo',$pseudo);
        if($requete->execute()) {
            $id = 1;
        }
    } catch (PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à rappers échouée</center>";
    }
    $requete = $pdo->prepare("SELECT id FROM rappers WHERE
        pseudo = :pseudo");
    try {
        $requete->bindValue(':pseudo',$pseudo);
        $requete->execute();
    } catch (PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à rappers échouée</center>";
    }
    if($result = $requete->fetch(PDO::FETCH_ASSOC)) {
		    $requete->closeCursor();
        $id = $result['id'];
	  }
    if($id > 0) {
        $i = 0;
        $j = 0;
        while($j < count($p_crew) && $i < count($crew))
        {
            if(in_array($p_crew[$j],$crew)) {
                $requete = $pdo->prepare("UPDATE crew
                    SET nom = :nom
                    WHERE rapper = :rapper AND nom = :p_nom");
                try {
                    $requete->bindValue(':rapper', $id);
                    $requete->bindValue(':nom',$crew[$i]);
                    $requete->bindValue(':p_nom',$p_crew[$j]);
                    $requete->execute();
                } catch (PDOException $ex) {
                    $erreurs_array = "<center>Erreur: Connexion à crew échouée</center>";
                }
                $i += 1;
            } else {
                delete_crew($id,$p_crew[$j],$erreurs_array);
            }
            $j += 1;
        }
        while($j < count($p_crew)) {
            delete_crew($id,$p_crew[$j],$erreurs_array);
            $j += 1;
        }
        while($i < count($crew)) {
            $requete = $pdo->prepare("INSERT INTO crew SET
                rapper = :rapper,
                nom = :nom");
            try {
                $requete->bindValue(':rapper', $id);
                $requete->bindValue(':nom',$crew[$i]);
                $requete->execute();
            } catch (PDOException $ex) {
                $erreurs_array = "<center>Erreur: Connexion à crew échouée</center>";
            }
            $i += 1;
        }
        if(count($erreurs_array) != 0)
        {
            return $id;
        }
    }
    return $requete->errorInfo();
}

function get_crew($crew, &$pseudos, &$erreurs_array) {
    $pdo = PDO2::getInstance();
    $ids = [];
    $requete = $pdo->prepare("SELECT id FROM crew WHERE
                                nom = :nom");
    try {
        $requete->bindValue(":nom", $crew);
        $requete->execute();
    } catch (PDOException $ex) {
        $erreurs_array[] = "<center>Erreur: Connexion à crew échouée</center>";
    }
    if($result = $requete->fetch(PDO::FETCH_ASSOC)) {
            $requete->closeCursor();
            $ids = $result['id'];
    }
    if(count($erreurs_array) != 0)
    {
        foreach($ids as $id){
            $requete = $pdo->prepare("SELECT pseudo FROM rappers WHERE
                                        id = :id");
            try {
                $requete->bindValue(':id',$id);
                $requete->execute();
            } catch (PDOException $ex) {
                $erreurs_array[] = "<center>Erreur: Connexion à rappers échouée</center>";
            }
            if($result = $requete->fetch(PDO::FETCH_ASSOC)) {
                $requete->closeCursor();
                $pseudos[] = $result['pseudo'];
            }
        }
    }
    if(count($erreurs_array) == 0)
    {
        return $id;
    }
    return $requete->errorInfo();
}
