<?php

include 'Admin/Global/config.php';
include 'Admin/libs/pdo2.php';

function add_visit($ip,&$erreurs_array) {
  $pdo = PDO2::getInstance();
  $id = 0;

  $requete = $pdo->prepare("INSERT INTO traffic SET
      ip = :ip");
  try {
    $requete->bindValue(':ip', $ip);
    if($requete->execute()) {
      $id = $pdo->lastInsertId();
    }
  } catch (PDOException $ex) {
    $erreurs_array[] = "<center> Erreur: Connexion à traffic échouée </center>";
  }
  if($id != 0) {
    return $id;
  }
  return $requete->errorInfo();
}

function get_visitor($ip,$erreurs_array) {
  $pdo = PDO2::getInstance();

  $requete = $pdo->prepare("SELECT ip FROM traffic WHERE
    ip = :ip");

  try {
    $requete->bindValue(':ip', $ip);
    $requete->execute();
  } catch (PDOException $ex) {
    $erreurs_array[] = "<center> Erreur: Connexion à traffic échouée </center>";
  }
  if(($result = $requete->fetch(PDO::FETCH_ASSOC)) > 0) {
    $requete->closeCursor();
    $ip = $result['ip'];
    return true;
  }
  return false;
}

function add_visit_m($ip,&$erreurs_array) {
  $pdo = PDO2::getInstance();
  $id = 0;

  $requete = $pdo->prepare("INSERT INTO traffic_m SET
      ip = :ip");
  try {
    $requete->bindValue(':ip', $ip);
    if($requete->execute()) {
      $id = $pdo->lastInsertId();
    }
  } catch (PDOException $ex) {
    $erreurs_array[] = "<center> Erreur: Connexion à traffic échouée </center>";
  }
  if($id != 0) {
    return $id;
  }
  return $requete->errorInfo();
}

function get_visitor_m($ip,$erreurs_array) {
  $pdo = PDO2::getInstance();

  $requete = $pdo->prepare("SELECT ip FROM traffic_m WHERE
    ip = :ip");

  try {
    $requete->bindValue(':ip', $ip);
    $requete->execute();
  } catch (PDOException $ex) {
    $erreurs_array[] = "<center> Erreur: Connexion à traffic échouée </center>";
  }
  if(($result = $requete->fetch(PDO::FETCH_ASSOC)) > 0) {
    $requete->closeCursor();
    $ip = $result['ip'];
    return true;
  }
  return false;
}
