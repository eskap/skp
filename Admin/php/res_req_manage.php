<?php

include '../Global/init.php';

include '../Modeles/admin.php';

$json_val = array();
$errreurs = array();
$crew = array();
$prenom = "";
$pseudo = "";

if(isset($_GET['type']) && $_GET['type'] == 'gerer_rapper')
{
    $pseudo = $_GET['pseudo'];
    get_rapper($pseudo, $prenom, $crew, $erreurs);
    if(count($erreurs) == 0) {
        $json_val['crew'] = $crew;
        $json_val['pseudo'] = $pseudo;
        $json_val['prenom'] = $prenom;
    }
    $json_val['erreurs'] = $erreurs;
    echo json_encode($json_val);
}

if(isset($_GET['type']) && $_GET['type'] == 'update_rappeur')
{
    $p_pseudo = $_GET['p_pseudo'];
    $p_name = $_GET['p_name'];
    $p_crew = $_GET['p_crew'];
    $pseudo = $_GET['pseudo'];
    $prenom = $_GET['prenom'];
    $crew = $_GET['crew'];
    update_rapper($p_pseudo, $p_name, $p_crew,  $prenom, $pseudo, $crew, $erreurs);
    $json_val['erreurs'] = $erreurs;
    echo json_encode($json_val);
}

if(isset($_GET['type']) && $_GET['type'] == 'delete_rapper')
{
    $p_pseudo = $_GET['p_pseudo'];
    $p_name = $_GET['p_name'];
    $p_crew = $_GET['p_crew'];
    delete_rapper($p_pseudo, $p_name, $p_crew, $erreurs);
    $json_val['erreurs'] = $erreurs;
    echo json_encode($json_val);
}
