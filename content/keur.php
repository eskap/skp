<div class="box-keur">
    <ul class="ul-keur">
        <li data-img="" data-title="A2H" class="li-keur first">
            <div class="li-keur-content none">
                <div class="grid">
                    <div class="col-1-1">
                        <div class="zone-text">
                            <h3> Lorem Ipsum</h3>
                            <p>
                                <iframe src="https://embed.spotify.com/?uri=spotify%3Aartist%3A2ktpq7bp4m1quizyEmp4r1" width="40%" height="400" frameborder="0" allowtransparency="true"></iframe> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a metus id augue tempor suscipit non in elit. Mauris feugiat bibendum tellus mollis venenatis. Fusce molestie augue vel sapien luctus luctus. Praesent congue, ligula sit amet vulputate eleifend, magna odio luctus neque, eu egestas massa leo vitae orci. Proin eleifend, neque non fermentum porttitor, dui nibh accumsan dui, sit amet luctus neque nulla et dui. Nam quam dolor, semper at massa sed, mollis tempus lectus. Suspendisse id tortor non turpis laoreet molestie. Vivamus ac quam vitae lacus sollicitudin eleifend a et ipsum. Morbi venenatis aliquam egestas. Fusce maximus fringilla commodo. Nam eu eros pretium, sollicitudin augue sit amet, sollicitudin nibh. Quisque porta diam eget lorem consequat, sit amet scelerisque justo fermentum. Nulla mattis odio urna, sed lobortis libero viverra a. Ut varius posuere turpis, sit amet sagittis massa facilisis sodales. Curabitur hendrerit laoreet metus, sed dignissim orci viverra vel. Cras faucibus vehicula nulla.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum. </p>
                        </div>
                    </div>
                </div>
                <div class="grid">
                    <div class="col-1-1">
                        <div class="h200 zone-text"> </div>
                    </div>
                </div>
            </div>
        </li>
        <li data-img="" data-title="Georgio" class="li-keur second">
            <div class="li-keur-content none">
                <div class="grid">
                    <div class="col-1-1">
                        <div class="zone-text">
                            <h3> Lorem Ipsum</h3>
                            <p>
                                <iframe src="https://embed.spotify.com/?uri=spotify%3Aartist%3A6Xc0KDqzw5u6EQLgdfeoKO" width="300" height="380" frameborder="0" allowtransparency="true"></iframe> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a metus id augue tempor suscipit non in elit. Mauris feugiat bibendum tellus mollis venenatis. Fusce molestie augue vel sapien luctus luctus. Praesent congue, ligula sit amet vulputate eleifend, magna odio luctus neque, eu egestas massa leo vitae orci. Proin eleifend, neque non fermentum porttitor, dui nibh accumsan dui, sit amet luctus neque nulla et dui. Nam quam dolor, semper at massa sed, mollis tempus lectus. Suspendisse id tortor non turpis laoreet molestie. Vivamus ac quam vitae lacus sollicitudin eleifend a et ipsum. Morbi venenatis aliquam egestas. Fusce maximus fringilla commodo. Nam eu eros pretium, sollicitudin augue sit amet, sollicitudin nibh. Quisque porta diam eget lorem consequat, sit amet scelerisque justo fermentum. Nulla mattis odio urna, sed lobortis libero viverra a. Ut varius posuere turpis, sit amet sagittis massa facilisis sodales. Curabitur hendrerit laoreet metus, sed dignissim orci viverra vel. Cras faucibus vehicula nulla.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum. </p>
                        </div>
                    </div>
                </div>
                <div class="grid">
                    <div class="col-1-1">
                        <div class="h200 zone-text"> </div>
                    </div>
                </div>
            </div>
        </li>
        <li data-img="" data-title="JeanJass" class="li-keur third">
            <div class="li-keur-content none">
                <div class="grid">
                    <div class="col-1-1">
                        <div class="zone-text">
                            <h3> Lorem Ipsum</h3>
                            <p>
                                <iframe src="https://embed.spotify.com/?uri=spotify%3Aartist%3A11jTIrOwxFOLvhJIdb4FYo" width="300" height="380" frameborder="0" allowtransparency="true"></iframe> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur a metus id augue tempor suscipit non in elit. Mauris feugiat bibendum tellus mollis venenatis. Fusce molestie augue vel sapien luctus luctus. Praesent congue, ligula sit amet vulputate eleifend, magna odio luctus neque, eu egestas massa leo vitae orci. Proin eleifend, neque non fermentum porttitor, dui nibh accumsan dui, sit amet luctus neque nulla et dui. Nam quam dolor, semper at massa sed, mollis tempus lectus. Suspendisse id tortor non turpis laoreet molestie. Vivamus ac quam vitae lacus sollicitudin eleifend a et ipsum. Morbi venenatis aliquam egestas. Fusce maximus fringilla commodo. Nam eu eros pretium, sollicitudin augue sit amet, sollicitudin nibh. Quisque porta diam eget lorem consequat, sit amet scelerisque justo fermentum. Nulla mattis odio urna, sed lobortis libero viverra a. Ut varius posuere turpis, sit amet sagittis massa facilisis sodales. Curabitur hendrerit laoreet metus, sed dignissim orci viverra vel. Cras faucibus vehicula nulla.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum.
                                <br>
                                <br> Nunc porta pellentesque justo, vel efficitur sem fringilla nec. Nulla molestie nunc est, at vestibulum turpis sagittis quis. Nulla blandit accumsan tellus quis dictum. Duis lacinia magna sit amet est finibus convallis. Curabitur eu diam auctor odio commodo hendrerit ut quis urna. Proin id ex magna. Vivamus mattis erat sed est tristique laoreet. Sed lobortis gravida lectus, id porta erat viverra a. Donec condimentum enim in dui gravida, sit amet sollicitudin mi dignissim. Nunc tempus id metus vitae pharetra. Pellentesque malesuada leo vitae congue vestibulum. </p>
                        </div>
                    </div>
                </div>
                <div class="grid">
                    <div class="col-1-1">
                        <div class="h200 zone-text"> </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>
<style>
    .box-keur ul {
        list-style: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        position: relative;
        background-color: white;
    }
    
    .box-keur .li-keur {
        box-sizing: border-box;
        width: 99%;
        background-color: #333;
        margin: 0;
        height: 200px;
        position: relative;
        cursor: pointer;
        transition: 0.2s all;
        margin: 5px;
        background-size: 100%;
        background-position: 15% 15%;
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
        overflow: hidden;
    }
    
    .box-keur .li-keur:hover {
        background-size: 120%;
        -webkit-filter: grayscale(0%);
        filter: grayscale(0%);
    }
    
    .box-keur h1 {
        margin: 0;
    }
    
    .box-keur .li-keur:before {
        content: attr(data-title);
        position: absolute;
        text-align: center;
        color: white;
        line-height: 200px;
        width: 100%;
        background-color: rgba(0, 0, 0, 0.5);
        transition: 0.2s all;
    }
    
    .box-keur .first {
        background-image: url("https://limpertinent93.files.wordpress.com/2014/06/a2h_jf14.jpg");
    }
    
    .box-keur .second {
        background-image: url("http://www.lechabada.com/site2015/wp-content/uploads/2016/02/concert-georgio-662.jpg");
    }
    
    .box-keur .third {
        background-image: url("http://www.qualidistrict.com/wp-content/uploads/2015/01/PHOTO-1-1024x682.jpg");
    }
    /*.li-keur:hover {
        background-size: 150%;
    }*/
    
    .box-keur .li-keur:hover:before {
        background-color: rgba(0, 0, 0, 0.5);
        font-size: 30px;
    }
    
    .box-keur .active {
        height: 550px;
        background-color: #333;
        background-size: 120%;
        background-color: white;
        cursor: default;
        background-image: none;
        border: 2px solid rgb(220, 220, 220);
        overflow-y: scroll;
    }
    
    .box-keur .active:before {
        line-height: 550px;
        height: 550px;
        background-color: transparent;
        content: "";
        z-index: -15;
        transition: 0s all;
    }
    
    .box-keur .active:hover:before {
        font-size: 30px;
        height: 540px;
        background-color: transparent;
    }
    
    .box-keur .retract {
        height: 40px;
    }
    
    .box-keur .retract:before {
        line-height: 40px;
        height: 40px;
    }
    
    .box-keur .li-keur-content {
        margin: 10px;
    }
    
    .box-keur iframe {
        float: right;
        margin: 0 0 0 15px;
        width: 40%;
        height: 400px;
        -webkit-filter: grayscale(100%);
        filter: grayscale(100%);
    }
    
    .none {
        display: none;
    }
    
    ::-webkit-scrollbar {
        width: 5px;
    }
    
    ::-webkit-scrollbar-thumb:vertical {
        margin: 5px;
        background-color: #999;
        -webkit-border-radius: 5px;
    }
    
    ::-webkit-scrollbar-button:start:decrement,
    ::-webkit-scrollbar-button:end:increment {
        height: 5px;
        display: block;
    }
</style>
<script>
    $(function () {
        $('li').on("click", function () {
            $('li').removeClass("active").removeClass("finish-keur").addClass("retract");
            $(this).removeClass("retract").addClass("finish-keur").addClass("active");
            $('li .li-keur-content').addClass("none");
            $(this).children(":first").removeClass("none");
        })
    });
</script>