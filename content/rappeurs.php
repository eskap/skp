<div style="margin-top:0px" class=" content rap-content">
    <div class="search-zone-rap">
        <!--<h3>Rappeurs</h3>
        
       -->
        <h3>Catégorie</h3>
        <div class="tag-zone" data-tag="">
            <input id="searchRap" name="search-rap" type="search" style="margin-left: 0px;">
            <div class="all-tag">
                <div data-actif="" class="tag">Cloud</div>
                <div data-search="" class="tag">Egotrip</div>
                <div data-search="" class="tag">Conscient</div>
                <div data-search="" class="tag">Engagé</div>
                <div data-search="" class="tag">Poétique</div>
                <div data-search="" class="tag">Hardcore</div>
                <div data-search="" class="tag">Commercial</div>
                <div data-search="" class="tag">Alternatif</div>
                <div data-search="" class="tag">Expérimentale</div>
                <div data-search="" class="tag">Gangsta</div>
            </div>
        </div>
        <p class="afficher_debug"></p>
        <div class="content-profile-rappeur">
            
        </div>
    </div>
    
    
    
    <!--<div class="grid all-rappeur">
        <div class="col-2-12">
            <div class="rappeur"></div>
        </div>
        <div class="col-2-12">
            <div class="rappeur"></div>
        </div>
        <div class="col-2-12">
            <div class="rappeur"></div>
        </div>
        <div class="col-2-12">
            <div class="rappeur"></div>
        </div>
        <div class="col-2-12">
            <div class="rappeur"></div>
        </div>
        <div class="col-2-12">
            <div class="rappeur"></div>
        </div>
    </div>-->
</div>
<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto');
    .rap-content * {
        font-family: Roboto;
        outline: none;
        
    }
    
    .rappeur{
        border: 1px solid #333;
        height: 250px;
        position: relative;
        overflow: hidden;
        margin-top: -1px;
        cursor: pointer;
        -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
        filter: grayscale(100%);
    }
    
    .rappeur:before {
        content: attr(data-pic);
        position: absolute;
        text-align: center;
        color: white;
        line-height: 450px;
        bottom: 0px;
        width: 100%;
        height: 100%;
        background-color: rgba(0,0,0,0.5);
        transition: 0.2s all;
        
    }
    
    .rappeur p{
        line-height: 415px;
        text-align: center;
    }
    
    .rappeur:hover:before{
        background-color: rgba(0,0,0,0.8);
    }
    
    .rap-content h1,
    .rap-content h1 {
        margin: 10px 5px;
    }
    
    .rap-content h3 {
        display: block;
        width: 96.5%;
        /*border-bottom: 1px solid rgba(51, 51, 51, 0.30);*/
        padding: 5px 0;
        margin-left: 5px;
        margin-bottom: 2px;
        margin-top: 2px;
    }
    
    .tag {
        border: 1px solid #333;
        float: left;
        margin: 5px;
        padding: 4px 8px;
        border-radius: 4px;
        font-size: 13px;
        cursor: pointer;
        transition: .2s all;
        position: relative;
        overflow: hidden;
    }
    
    .tag-zone .active {
        background: #222;
        color: white;
    }
    
    .tag:hover {
        color: white;
        background-color: #222;
    }
    /*.tag::before{
        content: "";
        position: absolute;
        width: 0%;
        height: 100%;
        top: 0;
        left: -10px;
        background-color: #333;
        transition: 0.3s all;
        transform: skew(-200deg);
        z-index: -1;
    }*/
    
    .tag:hover::before {
        width: 130%;
        transform: skew(200%);
    }
    
    .all-tag{
        overflow: hidden;
    }
    
    #searchRap {
        margin: 5px;
        border: 1px solid #b5b5b5;
        border-radius: 10px;
        padding: 5px 2px;
        padding-left: 10px;
        border: 1px solid #333;
        float: left;
        margin: 5px;
        padding: 4px 8px;
        border-radius: 4px;
        font-size: 13px;
    }
    
    #searchRap:focus {
        box-shadow: 0px 0px 10px rgba(103, 120, 255, 0.75);
        border: 1px solid rgba(103, 120, 255, 0.75);
    }
</style>
<script>
    $(function () {
        
        var tab_rappeur = 
        [{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        },{
            nom : "Ash Kidd",
            image : "image/ash_kidd.jpg",
            image_size : '230%',
            image_pos : '50%'
        },{
            nom : "Nekfeu",
            image: "image/Nekfeu.jpg",
            image_size : '130%',
            image_pos : '60% 100%'
            
        }];
        
        var i = 0;
        var d = 1;
        tab_rappeur.forEach(function(item,index){
            if(i == 0 || i%6 == 0){
                $(".content-profile-rappeur").append("<div style='width:98%' class='grid all-rappeur'></div>");
                $(".all-rappeur").last().append("<div class='col-2-12'><div data-src=profile data-pic='"+item.nom+"' class=rappeur style='background-image:url("+item.image+");background-position:"+item.image_pos+";background-size:"+item.image_size+";'></div></div>");
            }else{
                  $(".all-rappeur").last().append("<div class='col-2-12'><div  data-pic="+item.nom+"  class=rappeur style='background-image:url("+item.image+");background-position:"+item.image_pos+";background-size:"+item.image_size+";'></div></div>");
            }
            i++;
        });
        
        
        
        var w = $(window).height();
        $(".content-profile-rappeur").height(w - (w*0.28));
        $(".content-profile-rappeur").css("overflow-y","scroll");
        
        
        $.fn.pressEnter = function (fn) {
            return this.each(function () {
                $(this).bind('enterPress', fn);
                $(this).keyup(function (e) {
                    if (e.keyCode == 13) {
                        $(this).trigger("enterPress");
                    }
                })
            });
        };
        
        function popByValue(list,element){
            for(var i = 0;i < list.length; i++){
                if(element == list[i]){
                    list.splice(i,1);
                    return 1;
                }
            }
            return 0;
        }
        
        var tab = [];
        
        $('.tag').each(function(){
            var val = $(this).text();
            val = val.toLowerCase();
            $(this).attr("data-search",val);
            
        });
        
        $('.all-tag').on("click", ".tag", function () {
            var verif = $(this).attr("data-actif");
            if (verif == "true") {
                if ($(this).attr("data-add") == "after") {
                    $(this).fadeOut(300, function () {
                        $(this).remove();
                    });
                }
                else {
                    $(this).attr("data-actif", "false");
                    $(this).removeClass("active");
                    $(this).find(".x").remove();
                }
                popByValue(tab,$(this).data("search"));
                $(".tag-zone").attr("data-tag",tab);
            }
            else {
                $(this).attr("data-actif", "true");
                $(this).addClass("active");
                $(this).append("<span class='x'> X</span>");
                tab.push($(this).data("search"));
                $(".tag-zone").attr("data-tag",tab);
            }
            $(".rap-content p").html("Votre recherche avec les tags : "+tab+"");
        });
        
        
        $("#searchRap").pressEnter(function () {
            var val = $(this).val();
            val = val.toLowerCase();
            var i = 0;
            var new_val = "";
            while(i < val.length){
                var string = "";
                
                if(val[i] == "<"){
                    var a = 0;
                    while(i< val.length && val[i] != ">"){
                        string[a] = val[i];
                        //val.slice(i,1);
                        i++;
                        a++;
                    }
                }else{
                    console.log(val[i]);
                    new_val = new_val +(val[i]);
                }
                i++;
            }
            
            
            val = new_val.trim();
            if (val != "") {
                $('.all-tag').prepend("<div data-actif='true' data-add='after' class='tag active'>" + val + "<span class='x'> X</span></div>");
                $('.tag').first().attr("data-search",val);
                tab.push(val);
                $(this).val("");
                $(".tag-zone").attr("data-tag",tab);
            }else{
                alert("Tu as voulu mettre du code php ou html petit con ?");
                alert("Oui ?");
                alert("Tu vas le regretter FDP");
                for(var j = 0; j < 5; j++){
                     alert("Tu as voulu mettre du code php ou html petit con ?");
                    alert("Oui ?");
                    alert("Tu vas le regretter FDP");
                }
            }
             $(".rap-content p").html("Votre recherche avec les tags : "+tab+"");
        });
        
        function htmlEntities(str) {
            return String(str).replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
        }
        
    });
</script>