<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Alexandre Mtimet</title>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-2.2.4.js" integrity="sha256-iT6Q9iMJYuQiMWNd9lDyBUStIq/8PuOW33aOqmvFpqI=" crossorigin="anonymous"></script>
    <style>
        .diaporama {
            width: 98.5%;
            height: 80vh;
            border: 2px solid white;
            overflow: hidden;
            /* Remove this part if you want to add your own structure html*/
            font-family: lato;
        }
        
        .content-diap {
            width: 400%;
            overflow: hidden;
            color: white;
            /*border: 2px solid red;*/
        }
        
        .content-diap .diap {
            box-sizing: border-box;
            width: 25%;
            height: 80vh;
            /*margin-right: 5%;*/
            float: left;
        }
        
        .right-arrow {
            position: absolute;
            font-size: 30px;
            right: 2%;
            top: 50%;
            color: #333;
            cursor: pointer;
        }
        
        .left-arrow {
            position: absolute;
            font-size: 30px;
            left: 2%;
            top: 50%;
            color: #333;
            cursor: pointer;
        }
        
        #li-selector-news {
            position: absolute;
            list-style: none;
            margin: 0;
            margin-top: -40px;
            padding: 0;
            left: 50%;
            margin-left: -60px;
        }
        
        #li-selector-news li {
            width: 2vh;
            height: 2vh;
            border-radius: 100%;
            border: 2px solid white;
            float: left;
            margin: 5px;
            cursor: pointer;
        }
        
        #li-selector-news li.active {
            background-color: white;
        }
        
        .header {
            width: 90%;
            margin-left: 5%;
        }
        
        .form {
            float: right;
            margin-top: 5%;
            padding: 0;
            font-family: Lato;
            color: #333;
        }
        
        .diap {
            background-size: 130%;
            filter: grayscale(100%);
            -webkit-filter: grayscale(100%);
            text-shadow: 1px 0 0 black, -1px 0 0 black, 0 1px 0 black, 0 -1px 0 black;
        }
        
        .diap h3 {
            margin: 20px;
            font-size: 30px;
        }
        
        .diap p {
            font-size: 20px;
            margin-left: 15px;
            margin-top: 30px;
            display: block;
            width: 500px;
            display: relative;
            top: 50%;
        }
        
        .diap button {
            float: right;
            margin: 20px;
            position: relative;
            bottom: 70px;
            background-color: rgba(0, 0, 0, 0);
            border: 1px solid white;
            color: white;
        }
        
        .diap button:hover {
            color: #333;
            background-color: white;
        }
        
        .blackfilter {
            width: 100%;
            height: 100%;
            background: rgba(0, 0, 0, 0.5);
            overflow: hidden;
        }
        
        #second {
            background-image: url("http://oneyard.com/wp-content/uploads/2015/09/Nekfeu.jpg");
            background-position: 50%;
            background-size: 110%;
        }
        
        #first {
            background-image: url("https://pbs.twimg.com/media/CmCzqf3WYAALWca.jpg");
            background-position: 50%;
            background-size: 130%;
        }
        
        #fouth {
            background-image: url("https://pbs.twimg.com/media/C0DPlK-WIAENkcm.jpg");
            background-position: 50%;
            background-size: 130%;
        }
        
        #third {
            background-image: url("https://i.ytimg.com/vi/i-o-BAVzUnQ/maxresdefault.jpg");
            background-position: 50%;
            background-size: 140%;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            
            $('.load-article').on("click",function(){
                $("#content").load("content/articles.php").hide().fadeIn("slow");
            });
            
            
            
            i = 1;
            var val = 400;
            $("#" + i).addClass("active");
            $(":input").change(function () {
                val = $(this).val();
            });
            $(".right-arrow").click(function () {
                if (i < 4) {
                    $("#" + i).removeClass("active");
                    i = i + 1;
                    $("#" + i).addClass("active");
                    $(".content-diap").animate({
                        "margin-left": "-=100%"
                    }, val | 0);
                }
                verif(i);
            });
            $(".left-arrow").click(function () {
                if (i > 1) {
                    $("#" + i).removeClass("active");
                    i = i - 1;
                    $("#" + i).addClass("active");
                    $(".content-diap").animate({
                        "margin-left": "+=100%"
                    }, val | 0);
                }
                verif(i);
            });
            $('li').click(function () {
                if (!$(this).hasClass("active")) {
                    var e = $('.active').attr("id");
                    $('.active').removeClass("active");
                    var z = $(this).attr("id");
                    $(this).addClass('active');
                    if (e < z) {
                        $(".content-diap").animate({
                            "margin-left": "+=" + (100 * (e - z)) + "%"
                        }, val | 0);
                    }
                    else {
                        $(".content-diap").animate({
                            "margin-left": "+=" + (100 * (e - z)) + "%"
                        }, val | 0);
                    }
                    i = z | 0;
                }
                verif(i);
            });
            verif(i);
            
            
            
            

            function verif(i) {
                if (i == 1) {
                    $('.left-arrow').fadeOut();
                }
                else if (i != 1 && $('.left-arrow').not(":visible")) {
                    $('.left-arrow').fadeIn();
                }
                if (i == 4) {
                    $('.right-arrow').fadeOut();
                }
                else if (i != 4 && $('.right-arrow').not(":visible")) {
                    $('.right-arrow').fadeIn();
                }
            }
            
            
            
        });
    </script>
</head>

<body>
    <section>
        <div class="diaporama">
            <div class="content-diap">
                <div id="first" class="diap">
                    <div class="blackfilter">
                        <h3>Lorem ipsum</h3>
                        <p>Etiam ultrices urna nec velit iaculis, et tincidunt magna mollis. Duis cursus nisi vitae faucibus vehicula. Ut quis orci in ligula tincidunt dictum sed at dui. Curabitur eu sem mauris. Aenean hendrerit mi ac risus aliquet elementum. Suspendisse velit felis, tincidunt quis nulla quis, congue mattis nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi ut turpis maximus, faucibus nulla sed, lobortis est.</p>
                    </div>
                    <button class="load-article">Lire l'article >></button>
                </div>
                <div id="second" class="diap">
                    <div class="blackfilter">
                        <h3>Lorem ipsum</h3>
                        <p>Etiam ultrices urna nec velit iaculis, et tincidunt magna mollis. Duis cursus nisi vitae faucibus vehicula. Ut quis orci in ligula tincidunt dictum sed at dui. Curabitur eu sem mauris. Aenean hendrerit mi ac risus aliquet elementum. Suspendisse velit felis, tincidunt quis nulla quis, congue mattis nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi ut turpis maximus, faucibus nulla sed, lobortis est.</p>
                    </div>
                    <button class="load-article">Lire l'article >></button>
                </div>
                <div id="third" class="diap">
                    <div class="blackfilter">
                        <h3>Lorem ipsum</h3>
                        <p>Etiam ultrices urna nec velit iaculis, et tincidunt magna mollis. Duis cursus nisi vitae faucibus vehicula. Ut quis orci in ligula tincidunt dictum sed at dui. Curabitur eu sem mauris. Aenean hendrerit mi ac risus aliquet elementum. Suspendisse velit felis, tincidunt quis nulla quis, congue mattis nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi ut turpis maximus, faucibus nulla sed, lobortis est.</p>
                    </div>
                    <button class="load-article">Lire l'article >></button>
                </div>
                <div id="fouth" class="diap">
                    <div class="blackfilter">
                        <h3>Lorem ipsum</h3>
                        <p>Etiam ultrices urna nec velit iaculis, et tincidunt magna mollis. Duis cursus nisi vitae faucibus vehicula. Ut quis orci in ligula tincidunt dictum sed at dui. Curabitur eu sem mauris. Aenean hendrerit mi ac risus aliquet elementum. Suspendisse velit felis, tincidunt quis nulla quis, congue mattis nibh. Interdum et malesuada fames ac ante ipsum primis in faucibus. Morbi ut turpis maximus, faucibus nulla sed, lobortis est.</p>
                    </div>
                    <button class="load-article">Lire l'article >></button>
                </div>
            </div>
        </div>
    </section>
    <div class="right-arrow"> <i class="fa fa-arrow-right" aria-hidden="true"></i> </div>
    <div class="left-arrow"> <i class="fa fa-arrow-left" aria-hidden="true"></i> </div>
    <ul id="li-selector-news">
        <li id="1"></li>
        <li id="2"></li>
        <li id="3"></li>
        <li id="4"></li>
    </ul>
</body>

</html>