<!--<img src="image/playlist_little.png" style="
    witdh: 100%;
    WIDTH: 130%;
    /* margin: auto; */
    display: block;
    position: relative;
    left: -15%;
    margin-top: -10%;
"/>-->

<div class="content-playlist">
    <div id="one" class="pochette"></div>
    <div id="two" class="pochette"></div>
    <div id="three" class="pochette"></div>
    <div id="four" class="pochette"></div>
    <div id="five" class="pochette"></div>
    <div id="six" class="pochette"></div>
    <div id="seven" class="pochette"><iframe src="https://open.spotify.com/embed/user/alexandredu75mtimet/playlist/4LTtCsrhgeG3MnDr2zSLxg" width="80" height="80" frameborder="0" allowtransparency="true"></iframe></div>
    <div id="eight" class="pochette"></div>
    <div id="nine" class="pochette"></div>
    <div id="ten" class="pochette"></div>
    <div id="eleven" class="pochette"></div>
    <div class="playlist_describe">
        <h3>Playlist</h3>
        <p>Décrouver la séléction musicale concocter par l'équipe du site.</p>
    </div>
    <div class="content_arrow_vote">
        <i class="up fa fa-arrow-up" aria-hidden="true"></i>
        <p class="indactor_vote">2 likes</p>
        <i class="down fa fa-arrow-down" aria-hidden="true"></i>
    </div>
   
</div>


<style>
    .content-playlist{
        position: relative;
        overflow: hidden;
        width: 65em;
        margin: auto;
        height: 600px;
    }
    
    .content_arrow_vote{
        position: absolute;
        left: 860px;
        top: 60px;
    }
    
    i.up:hover{
        color: #2ecc71;
    }
    
    i.down:hover{
        color: #e74c3c;
    }
    
    .content_arrow_vote p{
        margin-left: -15px;
        font-weight: 100;
    }
    
    .content_arrow_vote i{
        cursor: pointer;
        transition: 0.2s all;
        font-weight: 100;
    }
    
    
    
    .playlist_describe{
        position: absolute;
        top: 430px;
        width: 250px;
        text-align: justify;
        /* margin: 10px; */
        left: 50px;
        font-weight: 100;
    }
    
    .content-playlist iframe{
        transform: scale(2.2);
        transform-origin: 2% 2%;
    }
    
    h3{
        margin: 5px 0px;
        font-weight: 200;
    }
    
    p{
        margin: 0px;
    }
    .pochette{
        position: absolute;
        width: 170px;
        height: 170px;
        border: 1px solid black;
        background-color:white;
        background-size: 101%;
    }
    
    .pochette:before{
        content: " ";
        position: absolute;
        top: 0;
        bottom: 0;
        background-color: rgba(0, 0, 0, 0.0);
        width: 100%;
        transition: 0.2s all;
    }
    
    .pochette:hover:before{
        background-color: rgba(0, 0, 0, 0.4);
    }
    
    #one{
        top: 340px;
        left: 340px;
        z-index: 8;
        background-image:url("image/playlist/2chezmoi.jpg");
    }
    
    #two{
        top: 250px;
        left: 235px;
        z-index: 7;
        background-image:url("image/playlist/500x500.1133032270576ac42b1a32f.png");
    }
    
    #three{
        top: 160px;
        left: 110px;
        z-index: 6;
        background-image:url("image/playlist/aHR0cHM6Ly9pLnNjZG4uY28vaW1hZ2UvY2VlMWJjYjQ0NWYxMWI2NzQwN2FjZDMzOWRlN2VjMjI5MmRhOTc2MA==.jpg");
    }
    
    #four{
        left: 220px;
        top: 30px;
        background-image:url("image/playlist/damso.jpg");
    }
    
    #five{
        left: 440px;
        background-image:url("image/playlist/feu.jpg");
    }
    
    #six{
        top: 120px;
        left: 325px;
        background-image:url("image/playlist/Lord-Esperanza-Reaphit.jpg");
    }
    
    #seven{
        top: 200px;
        left: 455px;
        z-index: 12;
        overflow: hidden;
        background-image:url("");
    }
    
    #seven:before{
        z-index: -1;
    }
    
    #eight{
        left: 545px;
        top: 120px;
        z-index: 9;
        background-image:url("image/playlist/agartha.jpg");
    }
    
    #nine{
        left: 660px;
        top: 19px;
        background-image:url("image/playlist/4537bf30d61e3d1f57712a5e3dd62a1a.450x450x1.jpg");
        
    }
    
    #ten{
        left: 690px;
        top: 230px;
        z-index: 10;
        background-image:url("image/playlist/7daa60f56ad85f84f77929c0e10b3d3d.960x960x1.jpg");
       
    }
    
    #eleven{
        top: 315px;
        left: 580px;
        z-index: 11;
        background-image: url(image/playlist/d4a589e18eef7a734b97a34dd1d65b3a.960x960x1.jpg);
    }
</style>
