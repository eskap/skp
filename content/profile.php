<div class="profile">
    <div class="grid">
        <div class="grid">
            <div class="col-6-12">
                <div style="margin-top: 50px;" class="grid">
                    <div class="col-4-12"></div>
                    <div class="col-4-12"></div>
                    <div class="col-4-12"></div>
                </div>
                <div class="r_content_outer" style="position:relative;top:100px">
                    <div class="r_content_inner">
                        <h1 class="r_name">Ash Kidd</h1>
                        <p class="r_description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam  id turpis  vitae risus malesuada  facilisis. Praesent  dignissim fermentum  massa et euismod. Phasellus efficitur diam scelerisque, pulvinar quam a, elementum lacus. Donec pharetra laoreet magna, quis mollis mi. Nunc fringilla, massa sed blandit laoreet, diam orci facilisis purus, eget vulputate felis ante ut eros. Vestibulum vel augue mi. Praesent eget tincidunt mauris. Integer euismod risus vel tellus commodo, eu auctor urna sodales. Proin ut lobortis eros, nec auctor neque. Sed posuere turpis at elit accumsan hendrerit. Proin et turpis quis est pulvinar vehicula in in eros. Duis et mi non dui pulvinar dapibus.
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-2-12">
                <div class="content-image-rappeur-bg">
                    <div class="content-image-rappeur-inner">
                        <img src="image/big.ash_kidd.png" alt="Ash Kidd">
                    </div>
                </div>
            </div>
            <div class="col-4-12 r_music">
                <h3 class="r_keur">Coup de Keür</h3>
                <!-- spotify the one -->
                <iframe src="https://open.spotify.com/embed/track/0Z0jRXWQANp97ISqZIjM4q" width="300" height="100" frameborder="0" allowtransparency="true"></iframe>
                <h3 class="r_playlist">Bibliothèque Musicale</h3>
                <!-- spotify playlist -->
                <iframe src="https://open.spotify.com/embed/artist/4YUcg2ee9TxMNdjhAxzZBF" width="300" height="400" frameborder="0" allowtransparency="true"></iframe>
            </div>
        </div>
    </div>
</div>
 


<style>
    .profile{
       /*position: relative;*/
    }
    .content-image-rappeur-bg{
        /*position: absolute;
        width: 50%;*/
        display: flex;
        align-items: center;
        justify-content: center;
    }
    
    .content-image-rappeur-inner{
        /*position: absolute;
        top: 420px;
        left: 52%;
        transform: translateX(-48%) translateY(-50%);*/
        z-index: 5;
        transition: 0.2s all;
        margin-top: 76px;
    }
    
    img{
        width: 400px;
    }
    
    h1.r_name,h3.r_keur, h3.r_playlist{
        font-weight: 100;
        margin: 5px 0px;
    }
    h1.r_name{
        text-align: center;
        line-height: 100px;
        margin: 0;
        font-size: 35px;
    }
    
    p.r_description{
        margin: 0 40px;
        line-height: 150%;
    }
    
</style>

<script>
/*$("body").on("mouseenter",".r_content_outer",function(){
   $(".content-image-rappeur-inner").css("margin-left","50px");
}).on("mouseleave",".r_content_outer",function(){
   $(".content-image-rappeur-inner").css("margin-left","-50px");
});


    
$("body").on("mouseenter",".r_music",function(){
   $(".content-image-rappeur-inner").css("margin-left","-50px");
}).on("mouseleave",".r_music",function(){
   $(".content-image-rappeur-inner").css("margin-left","100px");
});*/
    $(".content-image-rappeur-inner").hide();
    $(".r_name").css("font-size","0px");
     $(".r_music").hide();
     $(".r_content_outer").hide();
        setTimeout(function () {
            $(".content-image-rappeur-inner").show().addClass("animated bounceInUp");
        }, 1200);
        setTimeout(function () {
            $(".r_music").show().addClass("animated bounceInDown");
        }, 800);
        setTimeout(function () {
            $(".r_content_outer").show().addClass("animated bounceInLeft");
        }, 800);
    
        setTimeout(function () {
            $(".r_name").animate({fontSize :"34px"},300);
        }, 1400);
</script>




