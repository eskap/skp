<?php

function valid_connexion($username,$mdp,&$erreurs_array) {
  $pdo = PDO2::getInstance();

  $requete = $pdo->prepare("SELECT id FROM admins
    WHERE username = :username AND
          mdp = :mdp");

  try{
    $requete->bindValue(':username',$username);
    $requete->bindValue(':mdp',$mdp);
    $requete->execute();
  } catch(PDOException $ex) {
    $erreurs_array[] = "ERROR";
  }
  if($result = $requete->fetch(PDO::FETCH_ASSOC)) {
    $requete->closeCursor();
    return $result['id'];
  }
  return false;
}
