<?php

include 'libs/form.php';
include 'Modeles/admin_co.php';

$form_connexion = new Form('formulaire_connexion');

$form_connexion->method('POST');

$form_connexion->add('Text', 'username')
              ->placeholder("Username")
              ->id_('input_field');

$form_connexion->add('Password', 'mdp')
              ->placeholder("Password")
              ->id_('input_field');

$form_connexion->add('Submit', 'submit');

$form_connexion->bound($_POST);

$erreurs_connexion = array();

if($form_connexion->is_valid($_POST)) {
  list($username,$mdp) = $form_connexion->get_cleaned_data('username','mdp');

  $id = valid_connexion($username,$mdp,$erreurs_connexion);

  if($id && empty($erreurs_connexion)) {
    $_SESSION['username'] = $username;
    header('Location: ../Admin/index.php');
  } else {
    echo '<center><p>Connexion échouée</p></center>';
  }
}
