<?php

session_start();

ini_set('magic_quotes_runtime',0);

if(get_magic_quotes_gpc() == 1) {
  function remove_magic_quotes_gpc(&$value) {
    $value = stripslashes($value);
  }
  array_walk_recursive($_GET,'remove_magic_quotes_gpc');
  array_walk_recursive($_POST, 'remove_magic_quotes_gpc');
  array_walk_recursive($_COOKIE, 'remove_magic_quotes_gpc');
}

include 'libs/pdo2.php';

function user_connected() {
  return !empty($_SESSION['username']);
}
