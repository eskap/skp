$(function () {
    $(".back").hide();
    $('.menu').hide();
    $("#start").on("click", function () {
        $(".right").addClass("animated bounceOutRight");
        setTimeout(function () {
            $(".right").css("display", "none");
            
        }, 400);
       
        $(".menu").delay(200).show(0).addClass("animated bounceInLeft");
        setTimeout(function () {
            $(".menu").removeClass("animated bounceInLeft");
        }, 1000);
    });
    $('body').on("click",'.pic, .pic-low', function (e) {
        console.log("salut");
        var check = $(this).data("video");
        if (check == true) {
            var src = $(this).data("src");
            $(this).append("<iframe src=" + src + "?autoplay=1 frameborder='0git ' allowfullscreen width='100%' height='100%'></iframe>").addClass("finish");
        }
        else {
            var src = $(this).data("src");
            if (src) {
                stop();
                animCss("animated bounceOutRight", 500, $(".menu"), "animated bounceInLeft", 1000, $(".content"), src);
                $(".back").fadeIn("slow");
            }
        }
    });
    
    
    $('body').on('click','.rappeur',function(){
        var rappeur = $(this).data("pic");
        var src = $(this).data("src");
        animCss("animated bounceOutUp", 500, $(".content"), "animated bounceInDown", 1000, $(".content"), src);
        
        
        $(".back").fadeIn("slow");
    });
    
    $(".back").on("click", function () {
        if ($(this).attr("data-return") == "news") {
            $("#content").empty().load("content/news.php").hide().fadeIn("slow");
            $('.back').attr("data-return","");
        }
        else {
            $("#content").addClass("animated bounceOutRight");
            setTimeout(function () {
                $("#content").removeClass("animated bounceOutRight").empty();
                $(".menu").delay(0).show(1).addClass("animated bounceInLeft");
            }, 500)
            setTimeout(function () {
                $(".menu").removeClass("animated bounceInLeft");
            }, 1000);
            $(".back").hide();
        }
    });
    
   
});

function animCss(xentrance, xtime, xobject, yentrance, ytime, yobject, src, bool = 0) {
    xobject.addClass(xentrance);
    setTimeout(function () {
        $(".menu").hide(0);
        xobject.removeClass(xentrance);
        yobject.delay(ytime).show(0).addClass(yentrance)
        $("#content").load("content/" + src + ".php");
        setTimeout(function () {
            yobject.removeClass(yentrance);
        }, ytime);
    }, xtime);
}

function stop() {
    $("#video-grunt").empty().removeClass("finish");
}

